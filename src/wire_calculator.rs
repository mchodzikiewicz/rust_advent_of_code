#[derive(Debug)]
#[derive(PartialEq)]
#[derive(Copy, Clone)]
pub struct Point {
    pub x : i32,
    pub y : i32,
}

use Point as Move;

#[derive(Debug)]
#[derive(PartialEq)]
pub struct Line {
    begin : Point,
    end : Point,
    pub len_to_end : u32,
}

#[derive(PartialEq)]
enum Orientation {
    Horizontal,
    Vertical,
}

#[derive(Debug)]
#[derive(PartialEq)]
pub struct Intersection {
    point : Point,
    pub len1 : u32,
    pub len2 : u32,
}

fn parse_move(raw_move : &str) -> Option<Move> {
    let move_distance = raw_move[1..].parse().unwrap();
    match raw_move.chars().nth(0) {
        Some('U') => Some(Move{x: 0, y: move_distance}),
        Some('D') => Some(Move{x: 0, y: -move_distance}),
        Some('L') => Some(Move{x: -move_distance, y: 0}),
        Some('R') => Some(Move{x: move_distance, y: 0}),
        _ => None,
    }
}

pub fn create_move_list(raw_moves : Vec<&str>) -> Vec<Move> {
    raw_moves.iter().map(|m| parse_move(m).unwrap()).collect()
}

fn move_to_line(begin_pos : &Point, begin_len : u32, next_move : &Move) -> Line {
    Line{begin: *begin_pos, end: Point{x: begin_pos.x + next_move.x,y: begin_pos.y + next_move.y},len_to_end: begin_len + (next_move.x.abs() as u32) + (next_move.y.abs() as u32)}
}

pub fn convert_moves_to_lines(moves: &Vec<Move>) -> Vec<Line> {
    let mut lines = Vec::<Line>::new();
    let mut current_pos = Point{x: 0, y: 0};
    let mut current_len = 0;
    for m in moves {
        lines.push(move_to_line(&current_pos,current_len,m));
        current_pos = lines.last().unwrap().end;
        current_len = lines.last().unwrap().len_to_end;
    }
    lines
}

fn in_range(r1 : i32, r2 : i32, x: i32) -> bool {
    let high;
    let low;
    if r1 >= r2 { high = r1; low = r2; }
    else { high = r2; low = r1; }
    if x < low { return false; }
    if x > high { return false; }
    true
}

fn check_intersection(line1 : &Line, line2 : &Line) -> Option<Intersection> {
    if line1.begin.x == line1.end.x && line1.begin.y == line1.end.y { return None };
    if line2.begin.x == line2.end.x && line2.begin.y == line2.end.y { return None };
    let line1_orientation;
    let line2_orientation;
    if line1.begin.x == line1.end.x { line1_orientation =  Orientation::Vertical; }
    else { line1_orientation =  Orientation::Horizontal; }
    if line2.begin.x == line2.end.x { line2_orientation =  Orientation::Vertical; }
    else { line2_orientation =  Orientation::Horizontal; }

    if line1_orientation == line2_orientation { return None; }
    if line1_orientation == Orientation::Horizontal {
        if in_range(line1.begin.x,line1.end.x,line2.begin.x) {
            if in_range(line2.begin.y,line2.end.y,line1.begin.y) {
                let intersection_p = Point{x: line2.begin.x, y: line1.begin.y};
                let end1_to_intersection_len = (intersection_p.x - line1.end.x).abs() as u32;
                let end2_to_intersection_len = (intersection_p.y - line2.end.y).abs() as u32;
                return Some(Intersection{point: intersection_p, len1: line1.len_to_end-end1_to_intersection_len, len2: line2.len_to_end-end2_to_intersection_len});
            }
        }
    }
    if line1_orientation == Orientation::Vertical {
        if in_range(line2.begin.x,line2.end.x,line1.begin.x) {
            if in_range(line1.begin.y,line1.end.y,line2.begin.y) {
                let intersection_p = Point{x: line1.begin.x, y: line2.begin.y};
                let end2_to_intersection_len = (intersection_p.x - line2.end.x).abs() as u32;
                let end1_to_intersection_len = (intersection_p.y - line1.end.y).abs() as u32;
                return Some(Intersection{point: intersection_p, len1: line1.len_to_end-end1_to_intersection_len, len2: line2.len_to_end-end2_to_intersection_len});
            }
        }
    }
    None
}

pub fn find_intersections(wire1 : Vec<Line>, wire2: Vec<Line>) -> Vec<Intersection> {
    let mut intersections = Vec::<Intersection>::new();
    for w1 in wire1.iter() {
        for w2 in wire2.iter() {
            match check_intersection(&w1, &w2) {
                Some(i) => {
                    intersections.push(i);
                }
                None => {}
            }
        }
    }
    intersections
}

pub fn find_shortest_intersection(wire1: &Vec<&str>, wire2: &Vec<&str>) -> u32 {
    let wire_move1 = create_move_list(wire1.clone());
    let wire_move2 = create_move_list(wire2.clone());

    let wire_lines1 = convert_moves_to_lines(&wire_move1);
    let wire_lines2 = convert_moves_to_lines(&wire_move2);
    println!("Wire 1 is {:?}\nWire 2 is {:?}",wire_lines1,wire_lines2);

    let mut intersections = find_intersections(wire_lines1,wire_lines2);
    intersections.sort_by(|i1,i2| (i1.len1+i1.len2).partial_cmp(&(i2.len1+i2.len2)).unwrap());
    println!("Intersections are {:?}",intersections);

    intersections[0].len1+intersections[0].len2
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use crate::wire_calculator::*;
    #[test_case("J154" => None)]
    #[test_case("U0" => Some(Move{x: 0, y: 0}))]
    #[test_case("U10" => Some(Move{x: 0, y: 10}))]
    #[test_case("D0" => Some(Move{x: 0, y: 0}))]
    #[test_case("D10" => Some(Move{x: 0, y: -10}))]
    #[test_case("L0" => Some(Move{x: 0, y: 0}))]
    #[test_case("L10" => Some(Move{x: -10, y:0}))]
    #[test_case("R0" => Some(Move{x: 0, y: 0}))]
    #[test_case("R10" => Some(Move{x: 10, y: 0}))]
    fn single_move(raw_move : &str) -> Option<Move> {
        parse_move(raw_move)
    }

    #[test_case(vec!() => Vec::<Move>::new())]
    #[test_case(vec!("R10","U10") => vec!(Move{x: 10,y: 0},Move{x: 0,y: 10}))]
    #[test_case(vec!("U10","R3","D15",) => vec!(Move{x: 0, y: 10},Move{x: 3,y: 0},Move{x: 0, y: -15}))]
    fn moves(raw_moves : Vec<&str>) -> Vec<Move> {
        create_move_list(raw_moves)
    }

    #[test_case(Point{x: 0,y: 0},0,Move{x: 0,y: 0} => Line{begin: Point{x: 0,y: 0}, end: Point{x: 0,y: 0},len_to_end: 0})]
    #[test_case(Point{x: 10, y: 20},0,Move{x: 0,y: 0} => Line{begin: Point{x: 10,y: 20}, end: Point{x: 10,y: 20}, len_to_end: 0})]
    #[test_case(Point{x: 0,y: 0},0,Move{x: 0,y: 10} => Line{begin: Point{x: 0,y: 0}, end: Point{x: 0,y: 10},len_to_end: 10})]
    #[test_case(Point{x: 123,y: 234},200,Move{x: 0,y: 10} => Line{begin: Point{x: 123,y: 234}, end: Point{x: 123,y: 244},len_to_end: 210})]
    fn test_move_to_line(begin_pos : Point,begin_len: u32, next_move : Move) -> Line {
        move_to_line(&begin_pos, begin_len, &next_move)
    }

    #[test_case(&Line{begin: Point{x: 0,y: 0}, end: Point{x: 0,y: 0},len_to_end: 0},&Line{begin: Point{x: 0,y: 0}, end: Point{x: 0,y: 0},len_to_end: 0} => None)]
    #[test_case(&Line{begin: Point{x: 5,y: 0}, end: Point{x: 5,y: 10},len_to_end: 15},&Line{begin: Point{x: 0,y: 5}, end: Point{x: 10,y: 5},len_to_end: 15} => Some(Intersection{point: Point{x: 5,y: 5},len1: 10,len2: 10}))]
    #[test_case(&Line{begin: Point{x: 0,y: 5}, end: Point{x: 10,y: 5}, len_to_end: 15},&Line{begin: Point{x: 5,y: 0}, end: Point{x: 5,y: 10},len_to_end: 15} => Some(Intersection{point: Point{x: 5,y: 5},len1: 10,len2:10}))]
    #[test_case(&Line{begin: Point{x: 0,y: 20}, end: Point{x: 10,y: 20},len_to_end: 30},&Line{begin: Point{x: 5,y: 0}, end: Point{x: 5,y: 10},len_to_end: 15} => None)]
    #[test_case(&Line{begin: Point{x: 20,y: 0}, end: Point{x: 20,y: 10}, len_to_end: 30},&Line{begin: Point{x: 0,y: 5}, end: Point{x: 10,y: 5},len_to_end: 15} => None)]
    fn test_intersection(line1 : &Line, line2 : &Line) -> Option<Intersection> {
        check_intersection(line1,line2)
    }


    #[test_case(
        vec!(Line{begin: Point{x: 10,y: 10}, end: Point{x: 20,y: 10},len_to_end: 10},Line{begin: Point{x: 20,y: 10}, end: Point{x: 20,y: 20},len_to_end: 20})
        ,vec!(Line{begin: Point{x: 0,y: 0}, end: Point{x: 0,y: 5},len_to_end: 5},Line{begin: Point{x: 0,y: 5}, end: Point{x: 5,y: 5},len_to_end: 10})
        => Vec::<Intersection>::new()
    )]
    #[test_case(
    vec!(Line{begin: Point{x: 5,y: 0}, end: Point{x: 5,y: 5},len_to_end: 5},Line{begin: Point{x: 5,y: 5}, end: Point{x: 5,y: 20},len_to_end: 20})
    ,vec!(Line{begin: Point{x: 0,y: 0}, end: Point{x: 0,y: 3},len_to_end: 3},Line{begin: Point{x: 0,y: 3}, end: Point{x: 10,y: 3},len_to_end: 13})
    => vec!(Intersection{point: Point{x: 5,y: 3},len1: 5, len2: 13})
    )]
    #[test_case(
    vec!(Line{begin: Point{x: 5,y: 0}, end: Point{x: 5,y: 5},len_to_end: 5},
        Line{begin: Point{x: 5,y: 5}, end: Point{x: 2,y: 5},len_to_end: 8},
        Line{begin: Point{x: 2,y: 5}, end: Point{x: 2,y: 0},len_to_end: 13})
    ,vec!(Line{begin: Point{x: 0,y: 0}, end: Point{x: 0,y: 3},len_to_end: 3},
        Line{begin: Point{x: 0,y: 3}, end: Point{x: 10,y: 3},len_to_end: 13})
    => vec!(Intersection{point: Point{x: 5,y: 3},len1: 5, len2: 8},Intersection{point: Point{x: 2, y: 3},len1: 10,len2: 5})
    )]
    fn test_intersections(wire1: Vec<Line>, wire2: Vec<Line>) -> Vec<Intersection> {
        find_intersections(wire1,wire2)
    }

    #[test_case(vec!("U7","R6","D4","L4"),vec!("R8","U5","L5","D3") => 30)]
    #[test_case(vec!("R8","U5","L5","D3"),vec!("U7","R6","D4","L4") => 30)]
    #[test_case(vec!("R75","D30","R83","U83","L12","D49","R71","U7","L72"),vec!("U62","R66","U55","R34","D71","R55","D58","R83") => 610)]
    #[test_case(vec!("U62","R66","U55","R34","D71","R55","D58","R83"),vec!("R75","D30","R83","U83","L12","D49","R71","U7","L72") => 610)]
    #[test_case(vec!("R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"),vec!("U98","R91","D20","R16","D67","R40","U7","R15","U6","R7") => 410)]
    #[test_case(vec!("U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"),vec!("R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51") => 410)]
    fn test_end_to_end(wire1: Vec<&str>, wire2: Vec<&str>) -> u32 {
        find_shortest_intersection(&wire1, &wire2)
    }




}