use std::fs::File;
use std::path::Path;
use std::io::Read;

pub fn split_by_commas(input : &str) -> Vec<&str> {
    input.split(",").collect()
}

pub fn split_by_newlines(input : &str) -> Vec<&str> {
    input.split("\n").collect()
}

pub fn load(filename: impl AsRef<Path>) -> String {
    let mut file = File::open(filename).unwrap();
    let mut content = String::new();
    let _len = file.read_to_string( &mut content);
    content
}




#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
