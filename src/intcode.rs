#[derive(Debug,PartialEq)]
pub struct Program {
    pub code: Vec<i32>,
    pub io: Vec<i32>,
}

pub fn execute(input: &Program) -> Program {
    let mut code_m: Vec<i32> = input.code.to_vec();
    let mut input = input.io.iter();
    let mut output: Vec<i32> = vec!();
    let mut position = 0;
    while {
        let mut digits: Vec<_> = code_m[position].to_string().chars().map(|d| d.to_digit(10).unwrap()).rev().collect();
        for _i in digits.len()..4 {
            digits.push(0);
        }
        let digits = digits;
        let arg2_mode = digits[3] == 0;
        let arg1_mode = digits[2] == 0;
        let opcode = (digits[1]*10 + digits[0]) as i32;
        let arg1;
        let arg2;
        let res_pos;
        let mut command_len;
        match opcode {
            99 => {
                return Program { code: code_m, io: output }
            },
            1 | 2 => {
                arg1 = match arg1_mode {
                    false => Some(code_m[position + 1]),
                    true => Some(code_m[code_m[position + 1] as usize]),
                };
                arg2 = match arg2_mode {
                    false => Some(code_m[position + 2]),
                    true => Some(code_m[code_m[position + 2] as usize]),
                };
                res_pos = Some(code_m[position + 3]);
                command_len = 4;
            }
            3 | 4 => {
                arg1 = Some(code_m[position + 1 as usize]);
                arg2 = None;
                res_pos = None;
                command_len = 2;
            }
            5 | 6 => {
                arg1 = match arg1_mode {
                    false => Some(code_m[position + 1]),
                    true => Some(code_m[code_m[position + 1] as usize]),
                };
                arg2 = match arg2_mode {
                    false => Some(code_m[position + 2]),
                    true => Some(code_m[code_m[position + 2] as usize]),
                };
                res_pos = None;
                command_len = 3;
            }
            7 | 8 => {
                arg1 = match arg1_mode {
                    false => Some(code_m[position + 1]),
                    true => Some(code_m[code_m[position + 1] as usize]),
                };
                arg2 = match arg2_mode {
                    false => Some(code_m[position + 2]),
                    true => Some(code_m[code_m[position + 2] as usize]),
                };
                res_pos = Some(code_m[position + 3]);
                command_len = 4;
            }
            _ => panic!("Invalid opcode: {}", opcode),
        };
        match opcode {
            1 => {
                let res_val = arg1.unwrap() + arg2.unwrap();
                println!("{}: Add {} and {}, result is {} stored at {}", position, arg1.unwrap(), arg2.unwrap(), res_val, res_pos.unwrap());
                code_m[res_pos.unwrap() as usize] = res_val;
            }
            2 => {
                let res_val = arg1.unwrap() * arg2.unwrap();
                println!("{}: Multiply {} and {}, result is {} stored at {}",position, arg1.unwrap(), arg2.unwrap(), res_val, res_pos.unwrap());
                code_m[res_pos.unwrap() as usize] = res_val;
            }
            3 => {
                let inp = input.next().unwrap().clone();
                code_m[arg1.unwrap() as usize] = inp;
                println!("{}: Store {} to {}",position, inp,arg1.unwrap());
            }
            4 => {
                output.push( code_m[arg1.unwrap() as usize]);
                println!("{}: Output {} from {}",position, code_m[arg1.unwrap() as usize],arg1.unwrap());
            }
            5 => {
                let condition = arg1.unwrap() != 0;
                match condition {
                    true => {
                        println!("{}: Jump to {} because true",position,arg2.unwrap());
                        position = arg2.unwrap() as usize;
                        command_len = 0;
                    },
                    false => {
                        println!("{}: Not jumping to {} because false",position, arg2.unwrap());
                    },
                }
            }
            6 => {
                let condition = arg1.unwrap() == 0;
                match condition {
                    true => {
                        println!("{}: Jump to {} because false",position,arg2.unwrap());
                        position = arg2.unwrap() as usize;
                        command_len = 0;
                    },
                    false => {
                        println!("{}: Not jumping to {} because true",position, arg2.unwrap());
                    },
                }
            }
            7 => {
                if arg1.unwrap() < arg2.unwrap() {
                    println!("{}: Storing 1 at {} because {} < {}",position,res_pos.unwrap(),arg1.unwrap(),arg2.unwrap());
                    code_m[res_pos.unwrap() as usize] = 1;
                } else {
                    println!("{}: Storing 0 at {} because {} >= {}",position,res_pos.unwrap(),arg1.unwrap(),arg2.unwrap());
                    code_m[res_pos.unwrap() as usize] = 0;
                }
            }
            8 => {
                if arg1.unwrap() == arg2.unwrap() {
                    println!("{}: Storing 1 at {} because {} == {}",position,res_pos.unwrap(),arg1.unwrap(),arg2.unwrap());
                    code_m[res_pos.unwrap() as usize] = 1;
                } else {
                    println!("{}: Storing 1 at {} because {} != {}",position,res_pos.unwrap(),arg1.unwrap(),arg2.unwrap());
                    code_m[res_pos.unwrap() as usize] = 0;
                }
            }
            _ => panic!("{}: Invalid opcode that should have been filtered in previous match",position),
        }
        position += command_len;
        code_m.len() >= position + command_len
    } {}
    Program { code: code_m, io: output }
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    use crate::intcode::*;
    #[test_case(Program{code: vec![1,0,0,0,99],io: vec!()} => Program{code: vec![2,0,0,0,99],io: vec!()})]
    #[test_case(Program{code: vec![2,3,0,3,99],io: vec!()} => Program{code: vec![2,3,0,6,99],io: vec!()})]
    #[test_case(Program{code: vec![2,4,4,5,99,0],io: vec!()} => Program{code: vec![2,4,4,5,99,9801],io: vec!()})]
    #[test_case(Program{code: vec![1,1,1,4,99,5,6,0,99],io: vec!()} => Program{code: vec![30,1,1,4,2,5,6,0,99],io: vec!()})]

    #[test_case(Program{code: vec![3,0,4,0,99],io: vec!(10)} => Program{code: vec![10,0,4,0,99],io: vec!(10)})]

    #[test_case(Program{code: vec![1002,4,3,4,33],io: vec!()} => Program{code: vec![1002,4,3,4,99],io: vec!()})]
    #[test_case(Program{code: vec![1101,100,-1,4,0],io: vec!()} => Program{code: vec![1101,100,-1,4,99],io: vec!()})]

    #[test_case(Program{code: vec![3,9,8,9,10,9,4,9,99,-1,8],io: vec!(7)} => Program{code: vec![3,9,8,9,10,9,4,9,99,0,8],io: vec!(0)})]
    #[test_case(Program{code: vec![3,9,8,9,10,9,4,9,99,-1,8],io: vec!(8)} => Program{code: vec![3,9,8,9,10,9,4,9,99,1,8],io: vec!(1)})]
    #[test_case(Program{code: vec![3,9,8,9,10,9,4,9,99,-1,8],io: vec!(9)} => Program{code: vec![3,9,8,9,10,9,4,9,99,0,8],io: vec!(0)})]

    #[test_case(Program{code: vec![3,3,1108,-1,8,3,4,3,99],io: vec!(7)} => Program{code: vec![3,3,1108,-1,8,3,4,3,99],io: vec!(0)})]
    #[test_case(Program{code: vec![3,3,1108,-1,8,3,4,3,99],io: vec!(8)} => Program{code: vec![3,3,1108,-1,8,3,4,3,99],io: vec!(1)})]
    #[test_case(Program{code: vec![3,3,1108,-1,8,3,4,3,99],io: vec!(9)} => Program{code: vec![3,3,1108,-1,8,3,4,3,99],io: vec!(0)})]

    #[test_case(Program{code: vec![3,9,7,9,10,9,4,9,99,-1,8],io: vec!(7)} => Program{code: vec![3,9,7,9,10,9,4,9,99,1,8],io: vec!(1)})]
    #[test_case(Program{code: vec![3,9,7,9,10,9,4,9,99,-1,8],io: vec!(8)} => Program{code: vec![3,9,7,9,10,9,4,9,99,0,8],io: vec!(0)})]
    #[test_case(Program{code: vec![3,9,7,9,10,9,4,9,99,-1,8],io: vec!(9)} => Program{code: vec![3,9,7,9,10,9,4,9,99,0,8],io: vec!(0)})]

    #[test_case(Program{code: vec![3,3,1107,-1,8,3,4,3,99],io: vec!(7)} => Program{code: vec![3,3,1107,1,8,3,4,3,99],io: vec!(1)})]
    #[test_case(Program{code: vec![3,3,1107,-1,8,3,4,3,99],io: vec!(8)} => Program{code: vec![3,3,1107,-1,8,3,4,3,99],io: vec!(0)})]
    #[test_case(Program{code: vec![3,3,1107,-1,8,3,4,3,99],io: vec!(9)} => Program{code: vec![3,3,1107,-1,8,3,4,3,99],io: vec!(0)})]

    #[test_case(Program{code: vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9],io: vec!(0)} => Program{code: vec![3,12,6,12,15,1,13,14,13,4,13,99,0,0,1,9],io: vec!(0)})]
    #[test_case(Program{code: vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9],io: vec!(1)} => Program{code: vec![3,12,6,12,15,1,13,14,13,4,13,99,1,1,1,9],io: vec!(1)})]
    #[test_case(Program{code: vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9],io: vec!(100)} => Program{code: vec![3,12,6,12,15,1,13,14,13,4,13,99,100,1,1,9],io: vec!(1)})]

    #[test_case(Program{code: vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1],io: vec!(0)} => Program{code: vec![3,3,1105,0,9,1101,0,0,12,4,12,99,0],io: vec!(0)})]
    #[test_case(Program{code: vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1],io: vec!(1)} => Program{code: vec![3,3,1105,1,9,1101,0,0,12,4,12,99,1],io: vec!(1)})]
    #[test_case(Program{code: vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1],io: vec!(100)} => Program{code: vec![3,3,1105,100,9,1101,0,0,12,4,12,99,1],io: vec!(1)})]
    fn exec_test(code : Program) -> Program {
        execute(&code)
    }

    #[test_case(Program{code: vec![3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99],
                        io: vec!(7)}
             => 999)]

    #[test_case(Program{code: vec![3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99],
                        io: vec!(8)}
             => 1000)]
    #[test_case(Program{code: vec![3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99],
                        io: vec!(9)}
            => 1001)]
    fn check_return(code : Program) -> i32 {
        execute(&code).io[0]
    }


}