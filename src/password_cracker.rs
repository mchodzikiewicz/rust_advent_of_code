use std::prelude::*;

fn are_only_two_same_adjecent_digits(password: u32) -> bool {
    let string_passwd = password.to_string();
    let mut found_same : Vec<char> = vec!();
    let mut invalidate_same : Vec<char> = vec!();
    for i in 1..string_passwd.len()-1 {
        let element = string_passwd.chars().nth(i).unwrap();
        let element_before = string_passwd.chars().nth(i-1).unwrap();
        let element_after = string_passwd.chars().nth(i+1).unwrap();
        if (element == element_before) || (element == element_after) {
            if !invalidate_same.contains(&element) {
                found_same.push(element);
            }
        }
        if element == element_before && element == element_after {
            if found_same.contains(&element) {
                invalidate_same.push(element);
            }
        }
    }
    for i in found_same {
        if !invalidate_same.contains(&i) {
            return true;
        }
    }
    false
}

fn are_monotonic(password: u32) -> bool {
    let string_passwd = password.to_string();
    for i in 0..string_passwd.len()-1 {
        if string_passwd.as_bytes()[i] > string_passwd.as_bytes()[i+1] {
            return false;
        }
    }
    true
}

pub fn matches(password: u32) -> bool {
    are_only_two_same_adjecent_digits(password) && are_monotonic(password)
}


#[cfg(test)]
mod tests {
    use test_case::test_case;

    use crate::password_cracker::*;

    #[test_case(123456 => false)]
    #[test_case(113456 => true)]
    #[test_case(123443 => true)]
    #[test_case(124443 => false)]
    #[test_case(444331 => true)]
    #[test_case(444133 => true)]
    #[test_case(444433 => true)]
    #[test_case(443333 => true)]
    fn check_adj(pass: u32) -> bool {
        are_only_two_same_adjecent_digits(pass)
    }

    #[test_case(123456 => true)]
    #[test_case(113456 => true)]
    #[test_case(123443 => false)]
    fn check_mon(pass: u32) -> bool {
        are_monotonic(pass)
    }

    #[test_case(123456 => false)]
    #[test_case(113456 => true)]
    #[test_case(123443 => false)]
    #[test_case(111456 => false)]
    fn check_match(pass: u32) -> bool {
        matches(pass)
    }
}