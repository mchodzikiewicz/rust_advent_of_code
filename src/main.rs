use crate::intcode::*;

mod intcode;
mod file_loader;

fn main() {
    let file = file_loader::load("src/new_intcode.txt");
    let code : Vec<i32> = file_loader::split_by_commas(&file).iter().map(|x|x.parse().unwrap()).collect();
    let program = Program{code,io : vec!(5)};
    let result = execute(&program);

    println!("Result is {:?}",result.io);
}
